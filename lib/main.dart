import 'package:assessment1/login.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({ Key? key }) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LogIn(),
      
      theme: ThemeData(
          primarySwatch: Colors.pink,
          accentColor: Colors.grey,
          scaffoldBackgroundColor: Colors.grey[100],
      ),
    );
  }
}