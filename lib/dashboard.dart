import 'package:assessment1/assets.dart';
import 'package:assessment1/editprofile.dart';
import 'package:assessment1/login.dart';
import 'package:assessment1/signup.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
        centerTitle: true,
      ),

      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                    onPressed: () => {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const LogIn()))
                    },
                    child: const Text('Log In Page'),
                  ),
            ),

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                    onPressed: () => {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const SignUp()))
                    },
                    child: const Text('Sign Up Page'),
                  ),
            ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () => {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const Dashboard()))
                    },
                    child: const Text('Dashboard Page'),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                    onPressed: () => {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const EditProfile()))
                    },
                    child: const Text('Edit Profile Page'),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ElevatedButton(
                    onPressed: () => {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => const Assets()))
                    },
                    child: const Text('Assets Page'),
                  ),
                ),
          ],
        ), 
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          // Add your press here
          Navigator.push(context, MaterialPageRoute(builder: (context) => const EditProfile()))
        },
        child: const Icon(Icons.edit),
      ),
    );
  }
}